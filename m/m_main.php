<?php
//временный модель-контейнер
//константы
include_once '../settings.php';
$connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
$connect->query('SET charset utf8');


    //добавление новых новостей в бд
 function add_new_record() {
     $pathname = '../parts/news/';
     $db_date = date("Y-m-d", strtotime($_POST['date'])) ;
     mkdir($pathname.$db_date, 0777);
     $pathnamed = substr($pathname, 3).$db_date.'/';
     
     $db_title = $_POST['title'];
     $db_smalltext = $_POST['smalltext'];
     $db_content = $_POST['content'];
     
     $_FILES['smallimage']['name'] = 'smallimage.jpg';
     $_FILES['middleimage']['name'] = 'middleimage.jpg';
     $_FILES['img_for_content']['name'] = 'img_for_content.jpg';
     
     $db_smallimage =  $pathnamed.basename($_FILES['smallimage']['name']);
     $db_middleimage =  $pathnamed.basename($_FILES['middleimage']['name']);
     $db_img_for_content =  $pathnamed.basename($_FILES['img_for_content']['name']);
     
     copy($_FILES['smallimage']['tmp_name'],'../'.$pathnamed.basename($_FILES['smallimage']['name']));
     copy($_FILES['middleimage']['tmp_name'],'../'.$pathnamed.basename($_FILES['middleimage']['name']));
     copy($_FILES['img_for_content']['tmp_name'],'../'.$pathnamed.basename($_FILES['img_for_content']['name']));
     
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $to_db_adding = $connect->query('INSERT INTO news (date,title,smalltext,smallimage,middleimage,content,img_for_content) VALUES("'.$db_date.'","'.$connect->quote($db_title).'","'.$connect->quote($db_smalltext).'", "'.$db_smallimage.'", "'.$db_middleimage.'","'.$connect->quote($db_content).'","'.$db_img_for_content.'")');
     }
    //лобавление новых today_is
 function add_new_today() {
     $date_is_today = date("Y-m-d", strtotime($_POST['today_is_date'])) ;
     $text_is_today = $_POST['today_is_text'];
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $to_db_adding = $connect->query('INSERT INTO today_is (date,text) VALUES("'.$date_is_today.'","'.$text_is_today.'")');
     }
    //добавление новой афиши
 function add_new_afisha() {
     $date_afisha = date("Y-m-d", strtotime($_POST['afisha_date'])) ;
     $text_afisha = $_POST['afisha_text'];
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $connect->query('INSERT INTO afisha (date,text) VALUES("'.$date_afisha.'","'.$text_afisha.'")');
 }
   //добавить запись в гостевую книгу
 function add_new_message() {
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $author = $_POST['author'];
     $text = $_POST['text'];
     $email = $_POST['email'];
     $date = date("Y-m-d");
     $connect->query('INSERT INTO guest_book (date,author,text,email) VALUES("'.$date.'","'.$author.'","'.$text.'","'.$email.'")');
 }
   //Довавить фото и альбом
 function add_photo() {
     $title_of_album = $_POST['album_title'];
     $name_of_album = $_POST['name_of_album'] = date('Y-m-d');
     if (!file_exists('img/album_'.$name_of_album)) { mkdir('img/album_'.$name_of_album);}
     else { echo "Фотографии загрузятся в альбом сегодняшнего дня!"."<br>";
     }
    if (is_array($_FILES['photobat'])) {
    foreach ($_FILES['photobat']['name'] as $key => $value) {
        if (!$_FILES['photobat']['error'][$key]) {
            if (is_uploaded_file($_FILES['photobat']['tmp_name'][$key])) {
                if (move_uploaded_file($_FILES['photobat']['tmp_name'][$key], 'img/album_'.$name_of_album.'/'.$_FILES['photobat']['name'][$key])) {
                    echo 'Файл: '.$_FILES['photobat']['name'][$key].' загружен.<br />';
                }
            }
        }
    }
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $connect->query('INSERT INTO album (date, title) VALUES("'.date('Y-m-d').'","'.$title_of_album.'")');
 } }
 ?>

  <?php
  //Добавления
 if (isset($_POST['title']) && isset($_POST['date']) && isset($_POST['smalltext']) && isset($_FILES['smallimage']) && isset($_FILES['middleimage']) && isset($_POST['content']) && isset($_FILES['img_for_content'])) { add_new_record();}

 if (isset($_POST['today_is_date']) && isset($_POST['today_is_text'])) {add_new_today();}

 if (isset($_POST['afisha_date']) && isset($_POST['afisha_text'])) {add_new_afisha();}

 if (isset($_POST['author']) && isset($_POST['email']) && isset($_POST['text'])) {add_new_message();}

 if (isset($_FILES['photobat']) && isset($_POST['album_title'])) {add_photo();}

 ?>  