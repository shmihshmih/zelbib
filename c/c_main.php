<?php
include_once 'settings.php';
//controller
$page = '';
if (isset($_GET['page'])) {$page = $_GET['page'];} else {$page = 'index';};
    switch ($page) {
    case 'news':
            include 'parts/nav.php';
            include 'parts/news.php';
            include 'parts/footer.php';
            global $pager;
            $pager = 'Новости';

            break;
    case 'services':
            include 'parts/nav.php';
            include 'parts/services.php';
            include 'parts/footer.php';
            $pager = 'Услуги';

            break;
    case 'album':
            include 'parts/nav.php';
            include 'parts/photo_album.php';
            include 'parts/footer.php';
            $pager = 'Фотографии';
            break;
    case 'about_library':
            include 'parts/nav.php';
            include 'parts/about_library.php';
            include 'parts/footer.php';
            $pager = 'О библиотеке';
            break;
    case 'about_city':
            include 'parts/nav.php';
            include 'parts/about_city.php';
            include 'parts/footer.php';
            $pager = 'О городе';
            break;
    case 'guest_book':
            include 'parts/nav.php';
            include 'parts/guest_book.php';
            include 'parts/footer.php';
            $pager = 'Гостевая книга';
            break;
    case 'clubs':
            include 'parts/nav.php';
            include 'parts/header.php';
            include 'parts/clubs.php';
            include 'parts/footer.php';
            $pager = 'Клубы';
            break;
    case 'competitions':
            include 'parts/nav.php';
            include 'parts/header.php';
            include 'parts/competitions.php';
            include 'parts/footer.php';
            $pager = 'Конкурсы';
            break;
    case 'recommendations':
            include 'parts/nav.php';
            include 'parts/header.php';
            include 'parts/recommendations.php';
            include 'parts/footer.php';
            $pager = 'Рекомендации';
            break;
    case 'index': 
            include 'parts/nav.php';
            include 'parts/header.php';
            include 'parts/section.php';
            include 'parts/main.php';
            include 'parts/footer.php';
            $pager = 'Главная';
            break;
    case 'docs': 
            include 'parts/nav.php';
            include 'parts/header.php';
            include 'parts/section.php';
            include 'parts/docs.php';
            include 'parts/footer.php';
            $pager = 'Документы';
            break;
    case 'article':
            include 'parts/nav.php';
            include 'parts/article.php';
            include 'parts/footer.php';
            $pager = 'Статья';
        break;
    case 'admin':
            include 'parts/nav.php';
            include 'parts/admin.php';
            include 'parts/footer.php';
            $pager = 'Администраторская панель';
        break;
    default : 
        include 'parts/nav.php';
        include 'parts/header.php';
        echo '<img src="img/404.png" class="page404">';
        include 'parts/footer.php';
        $pager = 'СТраница не найдена';
            break;
           
}
?>   