
<?php
//константы
include_once 'settings.php';
//view's
include_once 'v/v_main.php';
?>
<!DOCTYPE html>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/reset.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <script src="js/main.js"></script>
        <title>Библиотека-филиал №30</title>
        <meta name="robots" content="all" />
        <meta name="generator" content="Библиотека-филиал №30" />
        <meta name="document-state" content="Dynamic" />
        <meta name="author" content="Anton Kashtanov" />
        <meta name="description" content="МБУ 'ЗМР ЦБС' Библиотека-филиал №30 дк Горького г.Зеленодольска" />
        <meta name="keywords" content="ЗМУ, ЦБС, ЗМР, библиотека, филиал, №30, дк Горького, г. Зеленодольск" />
        <meta property="business:contact_data:street_address" content="ул.Ленина 46"/>
        <meta property="business:contact_data:locality" content="Зеленодольск"/>
        <meta property="business:contact_data:country_name" content="Россия"/>
        <meta property="business:contact_data:email" content="zel30.bibl@yandex.ru"/>
        <meta property="business:contact_data:phone_number" content="+7(84371) 4-27-17"/>
        <meta property="business:contact_data:website" content="http://zelbib.ru"/>
<?php include 'parts/main_image.php';?>
<div class="main">              
    <?php 
    //контроллер  
    include_once 'c/c_main.php'; 
    ?>        
</div>