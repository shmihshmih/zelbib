
SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `u0181324_library`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `u0181324_library`;

/* Структура для таблицы `afisha`:  */

CREATE TABLE `afisha` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `text` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=MyISAM
AUTO_INCREMENT=21 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `album`:  */

CREATE TABLE `album` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `title` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=MyISAM
AUTO_INCREMENT=33 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_action`:  */

CREATE TABLE `core_action` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caption` VARCHAR(20) COLLATE utf8_general_ci NOT NULL,
  `code` VARCHAR(20) COLLATE utf8_general_ci NOT NULL COMMENT 'code',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=5 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_actrol`:  */

CREATE TABLE `core_actrol` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action` BIGINT(20) NOT NULL,
  `role` BIGINT(20) NOT NULL,
  `active` TINYINT(1) NOT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `action` USING BTREE (`action`),
  KEY `role` USING BTREE (`role`)
) ENGINE=InnoDB
AUTO_INCREMENT=5 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_role`:  */

CREATE TABLE `core_role` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `caption` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'role',
  `code` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'code',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_user`:  */

CREATE TABLE `core_user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `caption` VARCHAR(20) COLLATE utf8_general_ci NOT NULL COMMENT 'user',
  `fio` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL,
  `pass` BIGINT(20) NOT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_userrole`:  */

CREATE TABLE `core_userrole` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `role` BIGINT(20) NOT NULL,
  `user` BIGINT(20) NOT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `role` USING BTREE (`role`)
) ENGINE=InnoDB
AUTO_INCREMENT=2 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `guest_book`:  */

CREATE TABLE `guest_book` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `author` TEXT COLLATE utf8_general_ci NOT NULL,
  `text` TEXT COLLATE utf8_general_ci NOT NULL,
  `email` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=MyISAM
AUTO_INCREMENT=1379 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `news`:  */

CREATE TABLE `news` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `date` DATE DEFAULT NULL,
  `title` TEXT COLLATE utf8_general_ci,
  `smalltext` TEXT COLLATE utf8_general_ci,
  `smallimage` TEXT COLLATE utf8_general_ci,
  `middleimage` TEXT COLLATE utf8_general_ci,
  `content` LONGTEXT COLLATE utf8_general_ci,
  `img_for_content` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=MyISAM
AUTO_INCREMENT=53 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `today_is`:  */

CREATE TABLE `today_is` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `text` TEXT COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY USING BTREE (`id`)
) ENGINE=MyISAM
AUTO_INCREMENT=41 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;