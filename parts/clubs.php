<div class="clubs">
<h1>Профориентационный клуб "Ориентир"</h1>
<p>Клуб знакомит читателей и гостей библиотеки с различными профессиями и ремеслами.</p>
<p>Встречи проходят в плановом порядке.</p>
<p>от 15 и старше</p>
<h1>«Юный эрудит» (разной направленности)</h1>
<p>Детско-подростковый клуб, направленный на развитие и процветание человеческого ума.</p>
<p>Встречи проходят в плановом порядке.</p>
<p>от 7 до 14 лет</p>
</div>