
<div class="article_mod">
    <h1>Добавление статьи</h1>
    <fieldset>
    <form class="article_edit" action="m/m_main.php" method="post" enctype="multipart/form-data">
            <input type="text" name="title" maxlength="50" placeholder="Заголовок статьи" required>(одно предложение)
            <br>
            <input type="date" name="date" placeholder="Дата статьи" required>
            <br>
            <textarea name="smalltext" maxlength="100" placeholder="Краткая аннотация статьи" required></textarea>
            <br>
            Картинка для колонки новостей(smallimage): <input type="file" name="smallimage" required>(обязательно! 91х100 формат .jpg)
            <br>
            Картинка статьи для гдавной страницы(middleimage): <input type="file" name="middleimage" required>(обязательно! 150х150 формат .jpg)
            <br>
            Главная страница статьи(img_of_content): <input type="file" name="img_for_content" required>
            <br>
            <textarea name="content" placeholder="Текст статьи" required></textarea><br>
            <input type="submit" value="Добавить новость на сайт">
            <input type="reset" value="  ">
</form>
</fieldset>
</div>   
<div class="today_is_mod">
    <h1>Добавить что сегодня</h1>
    <fieldset>
        <form class="today_is_edit" action="m/m_main.php" method="post">
            Какая сегодня дата: <input type="date" name="today_is_date" required>
            <br>
            <input type="text" name="today_is_text" maxlength="50" placeholder="Что сегодня" required>
            <br>
            <input type="submit" value="Добавить что сегодня" required="">
            <input type="reset" value="  ">
        </form>
    </fieldset>
</div>
<div class="afisfa_mod">
    <h1>Добавление афиши</h1>
    <fieldset>
        <form class="afisha_edit" action="m/m_main.php" method="post">
            Дата мероприятия: <input type="date" name="afisha_date" required="">
            <br>
            <input type="text" name="afisha_text" placeholder="Текст афиши" maxlength="50" required="">
            <br>
            <input type="submit" value="Добавить в афишу" required="">
            <input type="reset" value="  ">
        </form>
    </fieldset>
</div>
<div class="album_mod">
    <h1>Добавление фотографий</h1>
    <fieldset>
        <form class="album_edit" action="m/m_main.php" method="post" enctype="multipart/form-data">
            <input type="text" name="album_title" placeholder="Заголовок к альбому" required="">
            <br>
            Выбрать фотографии(photobat): <input name="photobat[]" type="file" multiple required="">(не более 40!)(обязательно уменьшить!)
            <br>
    <input type="submit" value="Загрузить">
    <input type="reset" value="  ">
    </form>
    </fieldset>
</div>
<div class="clearfix"></div>