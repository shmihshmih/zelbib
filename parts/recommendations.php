<div class="recommendations">
    <h1>Рубрика "Фантастика"</h1>
<p> Категория "РОМАН" </p>
<ul>
<li>1966/1965 : "Дюна" (Dune) Френк Герберт (Frank Herbert)</li>
<li>1970/1969 : "Левая рука тьмы" (The Left Hand of Darkness) Урсула ле Гуин (Ursula K. Le Guin)</li>
<li>1971/1970 : "Мир-кольцо" (Ringworld) Ларри Нивен (Larry Niven)</li>
<li>1973/1972 : "Сами боги" (The Gods Themselves) Айзек Азимов (Isaac Asimov)</li>
<li>1974/1973 : "Свидание с Рамой" (Rendezvous with Rama) Артур Кларк (Arthur C. Clarke)</li>
<li>1975/1974 : "Обделённые"The Dispossessed by Урсула ле Гуин (Ursula K. Le Guin)</li>
<li>1976/1975 : "Бесконечная война" (The Forever War) Джо Холдеман (Joe Haldeman)</li>
<li>1978/1977 : "Врата" (Gateway) Фредерик Пол (Frederik Pohl)</li>
<li>1979/1978 : Dreamsnake by Vonda McIntyre (на русский не переводился; название можно перевести как "Змея мечты" или "Мечтазмей" какой-нибудь)</li>
<li>1980/1979 : "Фонтаны Рая" (The Fountains of Paradise) Артур Кларк (Arthur C. Clarke)</li>
<li>1984/1983 : "Звездный прилив" (Startide Rising) Дэвид Брин (David Brin)</li>
<li>1985/1984 : "Нейромант" (Neuromancer) Уильям Гибсон (William Gibson)</li>
<li>1986/1985 : "Игра Эндера" (Ender's Game) Орсон Скотт Кард (Orson Scott Card)</li>
<li>1987/1986 : "Говорящий с мертвыми" (Speaker for the Dead) Орсон Скотт Кард (Orson Scott Card)</li>
<li>1993/1992 : Doomsday Book by Connie Willis (на русском не издавался; название переводится как "Книга Судного Дня")</li>
<li>1998 : "Бесконечный мир" (Forever Peace) Джо Холдеман (Joe Haldeman)</li>
<li>2002 : "Американские боги" (American Gods) Нил Гейман (Neil Gaiman)</li>
<li>2004 : "Паладин душ" (Paladin of Souls) Лоис МакМастер Буджолд (Lois McMaster Bujold)</li>
<li>2008/2007 : "Союз еврейских полисменов" (The Yiddish Policemen's Union) Майкл Чабон (Шейбон) (Michael Chabon)</li>
<li>2010/2009 : "Заводная" (The Windup Girl) Паоло Бачигалупи (Paolo Bacigalupi)</li>
<li>2011/2010 : Blackout/All Clear by Connie Willis (на русском не издавалось; название можно перевести примерно так "Света нет/Путь свободен")</li>
<li>2012/2011 : Among Others by Jo Walton (на русском не издавалось, более того, этот автор ни разу не переводился на русский язык; название - неоконченная идиома "among other thing", что значит "между прочим", "наряду с...")</li>
</ul>
<p></p>
</div>