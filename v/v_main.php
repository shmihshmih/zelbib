<?php
include_once 'settings.php';
$connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
$connect->query('SET charset utf8');
$table_news = $connect->query('SELECT * FROM news ORDER BY -date');
    //функция забирает 3 последние новости из бд с измененной датой для инлекса
 function for_index_news($start = 0,$count = 3) {
        $ic = 0;
        while ($row = $GLOBALS['table_news']->fetch()) {
        $date_from_row = strtotime($row['date']);
        $number_of_month_to_name = date('n',$date_from_row);
        $number_of_day_in_month = date('j',$date_from_row);
        switch ($number_of_month_to_name) {
                    case '1':
                        $russian_month = 'января';
                        break;
                    case '2':
                        $russian_month = 'февраля';
                        break;
                    case '3':
                        $russian_month = 'марта';
                        break;
                    case '4':
                        $russian_month = 'апреля';
                        break;
                    case '5':
                        $russian_month = 'мая';
                        break;
                    case '6':
                        $russian_month = 'июня';
                        break;
                    case '7':
                        $russian_month = 'июля';
                        break;
                    case '8':
                        $russian_month = 'августа';
                        break;
                    case '9':
                        $russian_month = 'сентября';
                        break;
                    case '10':
                        $russian_month = 'октября';
                        break;
                    case '11':
                        $russian_month = 'ноября';
                        break;
                    case '12':
                        $russian_month = 'декабря';
                        break;
                }

        $need_date_for_main = $number_of_day_in_month.' '.$russian_month;
        echo 
          '<div class="news">
                        <img src="'.$row['middleimage'].'" alt="news_image">
                        <div class="title_for_box">'.$need_date_for_main.'</div>
                        <div class="news_text"><a href="?page=article&article='.$row['id'].'">'.substr($row['smalltext'],1,-1).'</a></div>
                    </div>'
          ;  
        $ic++;
        if ($ic == 3) {
            break;
        }
        }
 }
    //функция забирает все записи по убывающей дате и выводит в страницу новостей
 function for_news_news() {
        while ($row = $GLOBALS['table_news']->fetch()) {
            echo '<div class="box_for_title">
    <div class="title_for_box">'.$row['date'].'</div>
    <div class="img_for_news"><img src="'.$row['smallimage'].'"></div>
    <div class="mini_text_for_news">
        <a href="?page=article&article='.$row['id'].'">'
            .substr($row['smalltext'],1,-1).'
        </a>
    </div>
</div>';
        }
 }
    //фугкция выводит новость
 function news_article() {
               $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
               $connect->query('SET charset utf8');
               $content = $connect->query('SELECT * FROM news WHERE id ='.$_GET['article']); 
               while ($row = $content->fetch()) {
                   echo '<div class="title_of_news">'.substr($row['title'], 1,-1).'</div>'
                     .'<hr>'
                   .'<div class="author_of_article">Библиотека-филиал №30 г. Зеленодольска.<div class="date_of_article">'.$row['date'].'</div>'.'</div>'
                   
                   .'<img class="img_for_content" src="'.$row['img_for_content'].'"/>'
                     .'<div class="content">'. substr($row['content'], 1,-1).'</div>';
               }
 }
    //функция выводит какой сегодня день
 function today_is() {
     $date_today = date("Y-m-d");
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $today_is_arr = $connect->query('SELECT * FROM today_is WHERE date="'.$date_today.'"');
     while ($row = $today_is_arr->fetch()) {
     echo '<div class="today">
                        <div class="title_for_box">Сегодня:</div>
                        <div class="box_for_title">
                            <div class="today_number">'.$row['date'].'</div>
                            <div class="today_action">'.$row['text'].'</div>
                        </div>
                    </div>';
 } }
    //функция выводит актуальную афишу(5 последних, даже не актуальных)
 function actual_afisha() {
     $date_today = date("Y-m-d");
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $afisha_arr = $connect->query('SELECT * FROM afisha ORDER BY -date LIMIT 10');
     while ($row = $afisha_arr->fetch()) {
     echo '<div class="today_number">'.$row['date'].'</div>
          <div class="today_action">'.$row['text'].'</div>';
 } 
     }
    //функция выводит записи в гостевой книге
 function load_messages() {
     $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
     $connect->query('SET charset utf8');
     $messages_arr = $connect->query('SELECT * FROM guest_book ORDER BY -date');
     while ($row = $messages_arr->fetch()) {
     echo '<div class="message">'
         .'<div class="date_of_message">'
         .$row['date']
         .'</div>'
         .'<div class="author_of_message">'
         .$row['author']
         .'</div>'
         
         .'<div class="email">'
         .$row['email']
         .'</div>'
       .'<div class="text_of_message">'
         .$row['text']
         .'</div>'
         .'</div>';
 } 
 }
    //функция выводит фотоальбомы
 function get_photo() {
     $i=0;
     $directory =  scandir('img/');
     $files_arr = array();
     if (isset($_GET['album_number'])) {$album_number = $_GET['album_number'];}
        else {$album_number = 0;}
        echo '<div class="albums">';
     if (!isset($album_number) || $album_number === 0 || $album_number === NULL) {
     foreach ($directory as $file) { 
         if (strpos($file, "album_") === 0) {
             $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
             $connect->query('SET charset utf8');
             $album_number_from_db = $connect->query('SELECT id FROM album WHERE date="'.str_replace('album_','',$file).'"');
             while ($row = $album_number_from_db->fetch()) { $id = $row['id']; }
             $files_arr['name'][] = $file;
             $five = scandir('img/'.$files_arr['name'][$i]);
             echo '<div class="album"><div class="box_for_title"><div class="title_for_box"><a href="'
             .'?page=album&album_number='
               .$id
               .'">'
               .str_replace('album_','', $files_arr['name'][$i])
               .'</a></div>'
               . '<img src = "'
               .'img/'.$files_arr['name'][$i].'/'.$five[5]
               .'" />'
               
               . '</div></div>';
     $i++;}}
     echo '</div>Оригиналы фото можно запросить по почте.';
     }
     else {
         $connect = new PDO('mysql:host='.HOST.';dbname='.BD, USER, PASS);
         $connect->query('SET charset utf8');
         $album_date_from_db = $connect->query('SELECT * FROM album WHERE id="'.$album_number.'"');
         while ($row = $album_date_from_db->fetch()) {
         $all_photo = scandir('img/album_'.$row['date']);
         echo '<div class="title_of_news">'.$row['title'].'</div>';
         echo "<div class='mini_photo_container'>";
         $i=0;
         foreach ($all_photo as $key) {
             $all_photo['name'][] = $key;
             
             echo '<div class="photo_mini">'.'<img src="img/album_'.$row['date'].'/'.$all_photo['name'][$i].'">'.'</div>';
             $i++;
         }
         echo "</div>";
         }
         
     }
 }
    //получение хлебных крошек
 function get_breadcrumbs() {
     if ( isset($_GET['page']) ) {$_GET['page'] = $_GET['page'];} else {$_GET['page']='index';};
switch ($_GET['page']) {
    case 'news':
            $pager = 'Новости';
            break;
    case 'services':
            $pager = 'Услуги';
            break;
    case 'album':
            $pager = 'Фотографии';
            break;
    case 'about_library':
            $pager = 'О библиотеке';
            break;
    case 'about_city':
            $pager = 'О городе';
            break;
    case 'guest_book':
            $pager = 'Гостевая книга';
            break;
    case 'clubs':
            $pager = 'Клубы';
            break;
    case 'competitions':
            $pager = 'Конкурсы';
            break;
    case 'recommendations':
            $pager = 'Рекомендации';
            break;
    case 'index': 
            $pager = 'Главная';
            break;
    case 'docs': 
            $pager = 'Документы';
            break;
    case 'article':
            $pager = 'Статья';
        break;
    case 'admin':
            $pager = 'Администраторская панель';
        break;
    default : 
        $pager = 'Страница не найдена!';
}

    if (isset($_GET['page']) && $_GET['page'] != 'index' && !isset($_GET['article'])) {
        echo 'Навигация по сайту: <a href="?page=index" class="bread">Главная</a> >> ';
            if ($_GET['page'] == 'index') {} 
                else { echo '<a href="?page='.$_GET['page'].'" class="bread">'.$pager.'</a> '; } 
    } elseif (isset($_GET['page']) && isset($_GET['article'])) {
        echo 'Навигация по сайту: <a href="?page=index" class="bread">Главная</a> >> ';
        echo '<a href="?page=news" class="bread">Новости</a> ';
    }
    
 }
   //показать все документы для скачки
 function load_docs() {
     $docs_directory = scandir('docs/');
     $docs_arr = array();
     $i=0;
     echo '<div class="list_of_docs">';
     foreach ($docs_directory as $doc) {
         $docs_arr['name'][] = $doc;
         echo '<a href="docs/'.$docs_arr['name'][$i].'">'.$docs_arr['name'][$i].'</a><br>';
         $i++;
     }
     echo '</div>';
 }
?>